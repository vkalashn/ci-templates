# CI-Templates

This repo contains standard CI scripts for building docker images, doing tests and creating helm packages (according to https://docs.gitlab.com/ee/ci/yaml/includes.html)

For usage just create an standard git-ci yml and insert the include statements to the yaml from this folder.


# Helm

Helm will be generated automatically and pushed to harbor. "-tag" is replaced later on by the commit tag/branch if used.